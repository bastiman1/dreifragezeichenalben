import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import re
from tqdm import tqdm
import json
import dotenv
import os

dotenv.load_dotenv()

# Set up your credentials
client_id = os.getenv("CLIENT_ID")
client_secret = os.getenv("CLIENT_SECRET")

# Authenticate
client_credentials_manager = SpotifyClientCredentials(client_id=client_id, client_secret=client_secret)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)


# Search for the artist to get the artist's ID
artist_name = 'Die drei ???'  # Replace with the desired artist's name
result = sp.search(q='artist:' + artist_name, type='artist')
artist_id = result['artists']['items'][0]['id']

# Get the artist's albums
def get_all_albums(artist_id):
    albums = []
    offset = 0
    limit = 50
    
    while True:
        results = sp.artist_albums(artist_id, album_type='album', limit=limit, offset=offset)
        albums.extend(results['items'])
        
        # Check if there are more albums to be fetched
        if len(results['items']) == limit:
            offset += limit
        else:
            break

    return albums

all_albums = get_all_albums(artist_id)

fragezeichen_dict = {}
regexp = re.compile(r'\d\d\d\/*')
for album in tqdm(all_albums,desc="Extracting Track URLs", unit="album"):
    if regexp.search(album['uri']):
        tracks = sp.album_tracks(album['id'])
        first_track_link = tracks['items'][0]['external_urls']['spotify']
        fragezeichen_dict[album['uri']] = first_track_link
print(fragezeichen_dict)
#with open("fragezeichen_dict.json", "w") as outfile: 
 #   json.dump(fragezeichen_dict, outfile)
